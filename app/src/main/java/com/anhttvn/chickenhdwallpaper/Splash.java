package com.anhttvn.chickenhdwallpaper;

import android.content.Intent;
import android.content.IntentSender;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.util.Log;
import android.view.View;


import com.anhttvn.chickenhdwallpaper.databinding.ActivitySplashBinding;
import com.anhttvn.chickenhdwallpaper.util.ConfigUtil;
import com.anhttvn.chickenhdwallpaper.util.Gdpr;
import com.anhttvn.wallpaperlib.database.ConfigData;
import com.anhttvn.wallpaperlib.ui.HomeActivity;
import com.anhttvn.wallpaperlib.util.BaseActivity;
import com.anhttvn.wallpaperlib.util.Config;
import com.anhttvn.wallpaperlib.util.Connectivity;
import com.google.android.gms.tasks.Task;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.play.core.appupdate.AppUpdateInfo;
import com.google.android.play.core.appupdate.AppUpdateManager;
import com.google.android.play.core.appupdate.AppUpdateManagerFactory;
import com.google.android.play.core.install.InstallStateUpdatedListener;
import com.google.android.play.core.install.model.AppUpdateType;
import com.google.android.play.core.install.model.InstallStatus;
import com.google.android.play.core.install.model.UpdateAvailability;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Random;

public class Splash extends BaseActivity {
    private ActivitySplashBinding splashBinding;
    private int progress;
    private static final int MY_REQUEST_CODE = 500;

    private AppUpdateManager appUpdateManager;

    @Override
    public void init() {
        Config.FOLDER_DOWNLOAD = ConfigUtil.FOLDER_DOWNLOAD;
        Config.KEY_NOTIFICATION = ConfigUtil.KEY_NOTIFICATION;
        Config.KEY_WALLPAPER = ConfigUtil.KEY_WALLPAPER;
        Config.INFORMATION = ConfigUtil.INFORMATION;
        ConfigData.DATABASE = ConfigUtil.DATABASE;
        ConfigData.VERSION = ConfigUtil.VERSION;

        Config.APP_NAME = this.getString(R.string.app_name);
        Config.FOLDER_ASSETS = ConfigUtil.FOLDER_ASSETS;
        Config.BANNER_WALLPAPER = R.drawable.wallpaper;
        Config.BANNER_ID_ADS_APPLOVIN = this.getString(R.string.banner);
        Config.FULL_ID_ADS_APPLOVIN = this.getString(R.string.interstitial);
        Config.NATIVE_ID = this.getString(R.string.nativeId);
        Config.MREC_ID = this.getString(R.string.mrecId);

        showBanner();
        appUpdateManager = AppUpdateManagerFactory.create(this);

        Gdpr gdpr = new Gdpr(this);
        gdpr.setGdpr();

        if (Connectivity.isConnected(this)) {
            checkForUpdate();
        }
        this.goHome();

    }

    @Override
    public View contentView() {
        splashBinding = ActivitySplashBinding.inflate(getLayoutInflater());
        return splashBinding.getRoot();
    }


    private ArrayList<String> bannersAsset() {
        ArrayList<String> pathList = new ArrayList<>();
        try {
            String[] files = this.getAssets().list("wallpaper");
            assert files != null;
            for (String name : files) {
                pathList.add("wallpaper" + File.separator + name);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return pathList;
    }

    private void showBanner() {
        ArrayList<String> banners = bannersAsset();
        Random rand = new Random();

        InputStream inputstream= null;
        try {
            int index = rand.nextInt(banners.size());
            inputstream = this.getAssets().open(banners.get(index));
        } catch (IOException e) {
            e.printStackTrace();
        }
        Drawable drawable = Drawable.createFromStream(inputstream, null);
        splashBinding.banner.setBackground(drawable);
    }

    private void checkForUpdate() {
        Task<AppUpdateInfo> appUpdateInfoTask = appUpdateManager.getAppUpdateInfo();

// Checks that the platform will allow the specified type of update.
        appUpdateInfoTask.addOnSuccessListener(appUpdateInfo -> {
            if (appUpdateInfo.updateAvailability() == UpdateAvailability.UPDATE_AVAILABLE
                    && appUpdateInfo.isUpdateTypeAllowed(AppUpdateType.IMMEDIATE)) {

                try {
                    appUpdateManager.startUpdateFlowForResult(
                            appUpdateInfo,
                            AppUpdateType.IMMEDIATE,
                            this,
                            MY_REQUEST_CODE);
                } catch (IntentSender.SendIntentException e) {
                    goHome();
                    e.printStackTrace();
                }
            } else {
                goHome();
            }
        });

        appUpdateManager.registerListener(listener);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    InstallStateUpdatedListener listener = state -> {
        if (state.installStatus() == InstallStatus.DOWNLOADED) {
            popupSnackbarForCompleteUpdate();
        }
    };

    private void popupSnackbarForCompleteUpdate() {
        Snackbar snackbar =
                Snackbar.make(
                        findViewById(android.R.id.content),
                        "An update has just been downloaded.",
                        Snackbar.LENGTH_INDEFINITE);
        snackbar.setAction("INSTALL", view -> appUpdateManager.completeUpdate());
        snackbar.setActionTextColor(
                getResources().getColor(android.R.color.white));
        snackbar.show();
    }

    @Override
    protected void onStop() {
        super.onStop();
        appUpdateManager.unregisterListener(listener);
    }

    @Override
    protected void onResume() {
        super.onResume();
        appUpdateManager
                .getAppUpdateInfo()
                .addOnSuccessListener(appUpdateInfo -> {
                    if (appUpdateInfo.installStatus() == InstallStatus.DOWNLOADED) {
                        popupSnackbarForCompleteUpdate();
                    }
                });
    }

    protected void goHome() {
        splashBinding.progress.setVisibility(View.VISIBLE);
        splashBinding.numberProgress.setVisibility(View.VISIBLE);
        splashBinding.progress.setProgress(0);
        splashBinding.progress.setMax(100);

        new Thread(() -> {
            for (progress = 0; progress <= 100; progress++) {

                try {
                    runOnUiThread(() -> {
                        splashBinding.numberProgress.setText(progress + " % ");
                        splashBinding.progress.setProgress(progress);

                        if (progress >= 99) {
                            splashBinding.progress.setVisibility(View.GONE);
                            splashBinding.numberProgress.setVisibility(View.GONE);
                            Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent);
                            finish();
                        }
                    });
                    Thread.sleep(100);

                } catch (Exception ignored) {
                }
            }


        }).start();
    }
}
