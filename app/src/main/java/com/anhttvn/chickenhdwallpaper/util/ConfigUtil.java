package com.anhttvn.chickenhdwallpaper.util;

public class ConfigUtil {
    public static final String FOLDER_DOWNLOAD = "CHICKEN_WALLPAPER";
    public static final String FOLDER_ASSETS = "wallpaper";
    /**
     * api
     */
    public static String KEY_NOTIFICATION = "Notification";

    public static String KEY_WALLPAPER = "Wallpaper_v2";
    public static String INFORMATION ="Information";

//  Config Database

    public static  final String DATABASE = "ChickenWallpaper.db";
    public static  final int VERSION = 3;
}
